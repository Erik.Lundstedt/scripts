#!/usr/bin/env fennel



(local binds [
		"M-p"  "run LAUNCHER"
		"M-Return"  "run terminal"

		"// Exit serWM important to remember this one!"
		"M-S-e"  "exit

		// client management"
		"M-Left"  "cycle_client, Forward"
		"M-Right"  "cycle_client, Backward"
		"M-S-Left"  "drag_client, Forward"
		"M-S-Right"  "drag_client, Backward"
		"M-S-f"  "toggle_client_fullscreen, &Selector::Focused"
		"M-S-q"  "kill_client"
		;;M-s"  ""
		"// workspace management"
		"M-Tab"  "toggle_workspace"
		"M-A-period"  "cycle_workspace, Forward"
		"M-A-comma"  "cycle_workspace, Backward"

		"// Layout management"
		"M-A-Up"  "update_max_main, More"
		"M-A-Down"  "update_max_main, Less"
		"M-A-Right"  "update_main_ratio, More"
		"M-A-Left"  "update_main_ratio, Less"

;;		map: { "1", "2", "3", "4", "5", "6", "7", "8", "9" } to index_selectors9)  {
;;			"M-{}"  focus_workspace REF
;;			"M-S-{}"  client_to_workspace REF
;;		};
		]
	   )
