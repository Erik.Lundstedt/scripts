#!/bin/env fennel


(fn term [class title cmd args]
  (.. "kitty " " --class " class " --title " title " -1 " cmd (table.concat args " "))
  )

(local cfg
	   {
		;;-------------general---------------------
		:term term
		:editor "nvim "
		:fm "xplr "
;;-------------music-----------------------
		:music "ncmpcpp "
		:visualiser "cava "
		:volume "ncpamixer "
		}
	   )



(fn gencmd [cmd args]
  (cfg.term "float" cmd cmd args )
  )

(fn filepick []
  (table.concat
   [
	"$("
	(gencmd cfg.fm [""]);;["--print-pwd-as-result"])
	")"
	]
   )
  )

(local cmdList
	   [
		{:name "open terminal texteditor"     :cmd (gencmd cfg.editor     ["-l"])}
		{:name "open scratchfile in terminal texteditor"     :cmd (gencmd cfg.editor     ["-l" "~/.scratch"])}
;;		{:name "open folder in terminal texteditor"     :cmd (gencmd cfg.editor     ["-l" (filepick)])}
		{:name "music player control"     :cmd (gencmd cfg.music      [""])}
		{:name "change volume"            :cmd (gencmd cfg.volume     [""])}
		{:name "filemanager"              :cmd (gencmd cfg.fm         ["|xclip -selection primary"])}
		{:name "visualizer"               :cmd (gencmd cfg.visualiser [""])}
;;        {:name "start music daemon"   :cmd "mopidy&"}
		])

(fn genInput []
  (local outvar [])
  (each [i v (ipairs cmdList)]
	(local key (. cmdList i))
	(table.insert outvar (.. (table.concat [(. key :name) (. key :cmd)] ":") "\n"))
	)
  (table.concat outvar)
  )


(os.execute (.. "echo \"" (genInput) "\"" "|dmenu -d :" "|sh -"))
