button={}
mon=peripheral.wrap("top")
function sleep()
	screen()
end
function clearTable()
	button = {}
	clearscren()
end
function clearscren()
	term.setBackgroundColor(colors.black)
	term.clear()
end

function cursor(x,y)
	term.setCursorPos(x,y)
end

function menu(xc,yc,a,b,c,d,e)
	clearscren()
	cursor(xc,yc)
	print(a)
	cursor(xc,yc+1)
	print(b)
	cursor(xc,yc+2)
	print(c)
	cursor(xc,yc+3)
	print(d)
	cursor(xc,yc+4)
	print(e)
end

function drawmenu(color,name,x,y)
	term.setBackgroundColor(color)

	cursor(x,y)
	print("******")

	cursor(x,y+1)
	print("******")

	cursor(x,y)
	print(name)
	cursor(1,10)
end

function testFor(xmin,xmax,ymin,ymax,name)
	while true do
		event, param1, param2, param3 = os.pullEvent()
		if event == "mouse_click" then
			if param2 >= xmin and param2 <= xmax and param3 >= ymin and param3 <= ymax then --opens or closes the Iris
				term.setBackgroundColor(colors.black)
				print (name)
			end
		end
	end
end

function fillx(x,xmax,symbol)
	if x < xmax then
		cursor(x,y)
		print(symbol)
		x = x +1
	end
end

function drawBtn(color,text) -- draws the button to access the dialing menu
	x,y = term.getSize()
	for yc = y-3, y-1 do
		for xc = x/2-5, x/2 do
			if color == "lgray" then
				term.setBackgroundColor(colors.lightGray)
			else if color == "gray" then
				term.setBackgroundColor(colors.gray)
			else
				term.setBackgroundColor(colors.red)
			end
			term.setCursorPos(xc,yc)
			term.write(" ")
		end
	end
	term.setCursorPos(x/2-4, y-2)
	term.setTextColor(colors.black)
	term.write(text)
	term.setBackgroundColor (colors.black)
end
end

function event ()
	while true do
		event, param1, param2, param3 ,param4 = os.pullEvent()
		print (event,"  ",param1,"  ", param2,"  ",param3,"  ",param4)
	end
end
-- Where was the screen clicked
function getClick()
	event,side,x,y = os.pullEvent("mouse_click")
	--   print("x:"..x.." y:"..y)
	return x,y
end

function setTable(name, func, xmin, xmax, ymin, ymax,state)
	button[name] = {}
	button[name]["func"] = func
	button[name]["active"] = state
	button[name]["xmin"] = xmin
	button[name]["xmax"] = xmax
	button[name]["ymin"] = ymin
	button[name]["ymax"] = ymax
end

function fill(text, color, bData)
	term.setBackgroundColor(color)
	local yspot = math.floor((bData["ymin"] + bData["ymax"]) /2)
	local xspot = math.floor((bData["xmax"] - bData["xmin"] - string.len(text)) /2) +1
	for j = bData["ymin"], bData["ymax"] do
		term.setCursorPos(bData["xmin"], j)
		if j == yspot then
			for k = 0, bData["xmax"] - bData["xmin"] - string.len(text) +1 do
				if k == xspot then
					term.write(text)
				else
					term.write("*")
				end
			end
		else
			for i = bData["xmin"], bData["xmax"] do
				term.write("*")
			end
		end
	end
	term.setBackgroundColor(colors.black)
end

function screen()
	local currColor
	for name,data in pairs(button) do
		local on = data["active"]
		if on == true then currColor = colors.lime else currColor = colors.red end
		fill(name, currColor, data)
	end
end

function toggleButton(name)
	button[name]["active"] = not button[name]["active"]
	screen()
end

function toggle(name)
	--	if button[name]["active"] == false then button[name]["active"] = true else button[name]["active"] =false end
	print (button[name])
end

function flash(name)
	toggleButton(name)
	screen()
	sleep(0.15)
	toggleButton(name)
	screen()
end

function checkxy(x, y)
	for name, data in pairs(button) do
		if y>=data["ymin"] and  y <= data["ymax"] then
			if x>=data["xmin"] and x<= data["xmax"] then
				data["func"]()
				return true
				--data["active"] = not data["active"]
				--print(name)
			end
		end
	end
	return false
end
