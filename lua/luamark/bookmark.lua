-- -*- tab-width: 3; indent-tabs-mode: t; -*-
local json=require("json")

local bookmark={}

local marks={}

function bookmark.new(name, uri, alias)
	marks[name] = {}
	marks[name]["uri"] = uri
	marks[name]["alias"] = alias
	marks[name]["name"] = name
end

function bookmark.get(name)
	return marks[name]
end

function bookmark.getAll()
	for k,v in pairs(marks) do
		print(k)
	end
end


function bookmark.remove(name)
	marks[name]=""
	marks[name]=nil
end


function bookmark.load()
	file=io.open("/home/erik/.config/luamark/bookmarks.json","r")
	io.input(file)
	local markJson=io.read()
	io.close()
	marks = json.decode(markJson)
end
bookmark.load()


function bookmark.save()
	local markJson=json.encode(marks)
	file=io.open("/home/erik/.config/luamark/bookmarks.json","w")
	io.output(file)
	io.write(markJson.."\n")
	io.close()
end


return bookmark
