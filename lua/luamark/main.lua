#!/bin/env lua
-- -*- tab-width: 4; indent-tabs-mode: t; lua-indent-level: 4; lua-indent-string-contents: t; -*-

--[[
	DISCLAMER
	parts of this code is taken from direwolf20´s "button api" for computercraft
	which is under an unknown licence
	he does however state that it is ok to use he´s code in the video he made
	video:https://www.youtube.com/watch?v=1nuMDtmnEjg&t=18s
--]]

local bookmark=require("bookmark")
bookmark.load("/home/erik/.config/luamark/bookmarks.json")
--local switch = require("switch")
--bookmark.new("test","~/bin/","bin")

--bookmark.new("test1","~/cProgs/","CPrograms")



function put(name,path,alias)
	bookmark.new(name,path,alias)
	bookmark.save()
end

function get(name)
	print(bookmark.get(name)["uri"])
end

function getAll()
	bookmark.getAll()
end

function remove(name)
	bookmark.remove(name)
	bookmark.save()
end

function getAlias(name)
	print(bookmark.get(name)["alias"])
end




_G.switch = function(param, case_table)
	local case = case_table[param]
	if case then
		return case()
	end
	local def = case_table['default']
	return def and def() or nil
end

function man(funcName)
end

function help()




end





function main(arg)
	switch(arg[1],
		{
			["get"]=function()
				get(arg[2])
			end,
			["put"]=function()
				put(arg[2],arg[3],arg[4])
			end,
			["remove"]=function()
				remove(arg[2])
			end,
			["rm"]=function()
				remove(arg[2])
			end,
			["getAlias"]=function()
				getAlias(arg[2])
			end,
			["list"]=function()
				getAll()
			end,
			["default"]=function()print("err")end
	})
end
main(arg)

bookmark.save()
