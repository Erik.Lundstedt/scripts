<!-- -*- tab-width: 2; indent-tabs-mode: t; org-adapt-indentation: t; -*- -->

LUAMARK(1) Version 1.0 | Documentation

# NAME
**luamark** — simple bookmark manager written in lua
that can be used on its on or as a library


# SYNOPSIS

| **luamark** | **action** | [**name**] | [**path**] | [**alias**] |

# DESCRIPTION

**luamark** is a simple bookmark-manager written in lua
it can be used either directly from the terminal or as a lua library

as it stores the bookmarks in json-format it should also be fairly easy
to write a client in any language able to read/write/parse json

it should also be easy to write a frontend using,say fzf or dmenu
as the **list** action outputs a list of existing bookmarks separated by **\\n**

## actions

get **name**

:   Prints the path of the bookmark **[name]**.

getAlias **name**
:   Prints the alias of the bookmark **[name]**.

put **name** **path** **alias**

:   adds a bookmark **name** with a path **path** and alias **alias**.

[remove/rm] **name**

:   removes the bookmark **name**.

list 

prints a list of bookmarks separated with newlines

usefull for piping into **dmenu(1)**,**rofi(1)** or **fzf(1)**

## FILES

*~/.config/luamark/bookmarks.json*

:   file where the bookmarks are stored in json format


## AUTHOR

Erik Lundstedt <erik@lundstedt.it>

## SEE ALSO

**lua(1)**, **luac(1)**, **fzf**
