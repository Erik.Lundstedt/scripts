#!/usr/bin/env bash

#feh --class "float" $(grep "png")
sel=$(sed -E 's/^\[[[:digit:]]+\]://mg;t;d'|sed -E 's/(\(image\))//gm;t;d'|dmenu -n)
#echo $sel
#regex='(((http|https|ftp|gopher)|mailto)[.:][^ >"\t]*|www\.[-a-z0-9.]+)[^ .,;\t>">\):]'
#url=$(echo "$sel"|grep -Po "$regex" | dmenu -p "Go:" -w "$WINDOWID") || exit
feh --class "float" $sel
