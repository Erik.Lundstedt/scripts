;;; pack.el --- some seldom needed packages          -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Erik Lundstedt

;; Author: Erik Lundstedt <erik@ArchcraftPC>
;; Keywords: convenience, lisp, tools


(defalias  'package  'crafted-package-install-package)

(package 'helm-file-preview)
(package 'lsp-scheme)
(package 'scheme-complete)
(package 'transmission)
