#!/usr/bin/bash
emoji_list="+1	👍
-1	👎
100	💯
1234	🔢
8ball	🎱
a	🅰
ab	🆎
abc	🔤
abcd	🔡
accept	🉑
admission tickets	🎟
aerial tramway	🚡
airplane	✈
airplane arriving	🛬
airplane departure	🛫
alarm clock	⏰
alembic	⚗
alien	👽
ambulance	🚑
amphora	🏺
anchor	⚓
angel	👼
anger	💢
angry	😠
anguished	😧
ant	🐜
apple	🍎
aquarius	♒
aries	♈
arrow backward	◀
arrow double down	⏬
arrow double up	⏫
arrow down	⬇
arrow down small	🔽
arrow forward	▶
arrow heading down	⤵
arrow heading up	⤴
arrow left	⬅
arrow lower left	↙
arrow lower right	↘
arrow right	➡
arrow right hook	↪
arrow up	⬆
arrow up down	↕
arrow up small	🔼
arrow upper left	↖
arrow upper right	↗
arrows clockwise	🔃
arrows counterclockwise	🔄
art	🎨
articulated lorry	🚛
astonished	😲
athletic shoe	👟
atm	🏧
atom symbol	⚛
avocado	🥑
b	🅱
baby	👶
baby bottle	🍼
baby chick	🐤
baby symbol	🚼
back	🔙
bacon	🥓
badminton racquet and shuttlecock	🏸
baggage claim	🛄
baguette bread	🥖
balloon	🎈
ballot box with ballot	🗳
ballot box with check	☑
bamboo	🎍
banana	🍌
bangbang	‼
bank	🏦
bar chart	📊
barber	💈
barely sunny	🌥
baseball	⚾
basketball	🏀
bat	🦇
bath	🛀
bathtub	🛁
battery	🔋
beach with umbrella	🏖
bear	🐻
bed	🛏
bee	🐝
beer	🍺
beers	🍻
beetle	🐞
beginner	🔰
bell	🔔
bellhop bell	🛎
bento	🍱
bicyclist	🚴
bike	🚲
bikini	👙
biohazard sign	☣
bird	🐦
birthday	🎂
black circle	⚫
black circle for record	⏺
black heart	🖤
black joker	🃏
black large square	⬛
black left pointing double triangle with vertical bar	⏮
black medium small square	◾
black medium square	◼
black nib	✒
black right pointing double triangle with vertical bar	⏭
black right pointing triangle with double vertical bar	⏯
black small square	▪
black square button	🔲
black square for stop	⏹
blond-haired-man	👱‍♂️
blond-haired-woman	👱‍♀️
blossom	🌼
blowfish	🐡
blue book	📘
blue car	🚙
blue heart	💙
blush	😊
boar	🐗
boat	⛵
bomb	💣
book	📖
bookmark	🔖
bookmark tabs	📑
books	📚
boom	💥
boot	👢
bouquet	💐
bow	🙇
bow and arrow	🏹
bowling	🎳
boxing glove	🥊
boy	👦
bread	🍞
bride with veil	👰
bridge at night	🌉
briefcase	💼
broken heart	💔
bug	🐛
building construction	🏗
bulb	💡
bullettrain front	🚅
bullettrain side	🚄
burrito	🌯
bus	🚌
busstop	🚏
bust in silhouette	👤
busts in silhouette	👥
butterfly	🦋
cactus	🌵
cake	🍰
calendar	📆
call me hand	🤙
calling	📲
camel	🐫
camera	📷
camera with flash	📸
camping	🏕
cancer	♋
candle	🕯
candy	🍬
canoe	🛶
capital abcd	🔠
capricorn	♑
car	🚗
card file box	🗃
card index	📇
card index dividers	🗂
carousel horse	🎠
carrot	🥕
cat	🐱
cat2	🐈
cd	💿
chains	⛓
champagne	🍾
chart	💹
chart with downwards trend	📉
chart with upwards trend	📈
checkered flag	🏁
cheese wedge	🧀
cherries	🍒
cherry blossom	🌸
chestnut	🌰
chicken	🐔
children crossing	🚸
chipmunk	🐿
chocolate bar	🍫
christmas tree	🎄
church	⛪
cinema	🎦
circus tent	🎪
city sunrise	🌇
city sunset	🌆
cityscape	🏙
cl	🆑
clap	👏
clapper	🎬
classical building	🏛
clinking glasses	🥂
clipboard	📋
clock1	🕐
clock10	🕙
clock1030	🕥
clock11	🕚
clock1130	🕦
clock12	🕛
clock1230	🕧
clock130	🕜
clock2	🕑
clock230	🕝
clock3	🕒
clock330	🕞
clock4	🕓
clock430	🕟
clock5	🕔
clock530	🕠
clock6	🕕
clock630	🕡
clock7	🕖
clock730	🕢
clock8	🕗
clock830	🕣
clock9	🕘
clock930	🕤
closed book	📕
closed lock with key	🔐
closed umbrella	🌂
cloud	☁
clown face	🤡
clubs	♣
cocktail	🍸
coffee	☕
coffin	⚰
cold sweat	😰
comet	☄
compression	🗜
computer	💻
confetti ball	🎊
confounded	😖
confused	😕
congratulations	㊗
construction	🚧
construction worker	👷
control knobs	🎛
convenience store	🏪
cookie	🍪
cool	🆒
cop	👮
copyright	©
corn	🌽
couch and lamp	🛋
couple	👫
couple with heart	💑
couplekiss	💏
cow	🐮
cow2	🐄
crab	🦀
credit card	💳
crescent moon	🌙
cricket bat and ball	🏏
crocodile	🐊
croissant	🥐
crossed flags	🎌
crossed swords	⚔
crown	👑
cry	😢
crying cat face	😿
crystal ball	🔮
cucumber	🥒
cupid	💘
curly loop	➰
currency exchange	💱
curry	🍛
custard	🍮
customs	🛃
cyclone	🌀
dagger knife	🗡
dancer	💃
dancers	👯
dango	🍡
dark sunglasses	🕶
dart	🎯
dash	💨
date	📅
deciduous tree	🌳
deer	🦌
department store	🏬
derelict house building	🏚
desert	🏜
desert island	🏝
desktop computer	🖥
diamond shape with a dot inside	💠
diamonds	♦
disappointed	😞
disappointed relieved	😥
dizzy	💫
dizzy face	😵
do not litter	🚯
dog	🐶
dog2	🐕
dollar	💵
dolls	🎎
dolphin	🐬
door	🚪
double vertical bar	⏸
doughnut	🍩
dove of peace	🕊
dragon	🐉
dragon face	🐲
dress	👗
dromedary camel	🐪
drooling face	🤤
droplet	💧
drum with drumsticks	🥁
duck	🦆
dvd	📀
e-mail	📧
eagle	🦅
ear	👂
ear of rice	🌾
earth africa	🌍
earth americas	🌎
earth asia	🌏
egg	🥚
eggplant	🍆
eight	8⃣
eight pointed black star	✴
eight spoked asterisk	✳
eject	⏏
electric plug	🔌
elephant	🐘
email	✉
end	🔚
envelope with arrow	📩
euro	💶
european castle	🏰
european post office	🏤
evergreen tree	🌲
exclamation	❗
expressionless	😑
eye	👁
eye-in-speech-bubble	👁️‍🗨️
eyeglasses	👓
eyes	👀
face palm	🤦
face with cowboy hat	🤠
face with head bandage	🤕
face with rolling eyes	🙄
face with thermometer	🤒
facepunch	👊
factory	🏭
fallen leaf	🍂
family	👪
fast forward	⏩
fax	📠
fearful	😨
feet	🐾
female sign	♀
female-artist	👩‍🎨
female-astronaut	👩‍🚀
female-construction-worker	👷‍♀️
female-cook	👩‍🍳
female-detective	🕵️‍♀️
female-doctor	👩‍⚕️
female-factory-worker	👩‍🏭
female-farmer	👩‍🌾
female-firefighter	👩‍🚒
female-guard	💂‍♀️
female-judge	👩‍⚖️
female-mechanic	👩‍🔧
female-office-worker	👩‍💼
female-pilot	👩‍✈️
female-police-officer	👮‍♀️
female-scientist	👩‍🔬
female-singer	👩‍🎤
female-student	👩‍🎓
female-teacher	👩‍🏫
female-technologist	👩‍💻
fencer	🤺
ferris wheel	🎡
ferry	⛴
field hockey stick and ball	🏑
file cabinet	🗄
file folder	📁
film frames	🎞
film projector	📽
fire	🔥
fire engine	🚒
fireworks	🎆
first place medal	🥇
first quarter moon	🌓
first quarter moon with face	🌛
fish	🐟
fish cake	🍥
fishing pole and fish	🎣
fist	✊
five	5⃣
flag-ac	🇦🇨
flag-ad	🇦🇩
flag-ae	🇦🇪
flag-af	🇦🇫
flag-ag	🇦🇬
flag-ai	🇦🇮
flag-al	🇦🇱
flag-am	🇦🇲
flag-ao	🇦🇴
flag-aq	🇦🇶
flag-ar	🇦🇷
flag-as	🇦🇸
flag-at	🇦🇹
flag-au	🇦🇺
flag-aw	🇦🇼
flag-ax	🇦🇽
flag-az	🇦🇿
flag-ba	🇧🇦
flag-bb	🇧🇧
flag-bd	🇧🇩
flag-be	🇧🇪
flag-bf	🇧🇫
flag-bg	🇧🇬
flag-bh	🇧🇭
flag-bi	🇧🇮
flag-bj	🇧🇯
flag-bl	🇧🇱
flag-bm	🇧🇲
flag-bn	🇧🇳
flag-bo	🇧🇴
flag-bq	🇧🇶
flag-br	🇧🇷
flag-bs	🇧🇸
flag-bt	🇧🇹
flag-bv	🇧🇻
flag-bw	🇧🇼
flag-by	🇧🇾
flag-bz	🇧🇿
flag-ca	🇨🇦
flag-cc	🇨🇨
flag-cd	🇨🇩
flag-cf	🇨🇫
flag-cg	🇨🇬
flag-ch	🇨🇭
flag-ci	🇨🇮
flag-ck	🇨🇰
flag-cl	🇨🇱
flag-cm	🇨🇲
flag-cn	🇨🇳
flag-co	🇨🇴
flag-cp	🇨🇵
flag-cr	🇨🇷
flag-cu	🇨🇺
flag-cv	🇨🇻
flag-cw	🇨🇼
flag-cx	🇨🇽
flag-cy	🇨🇾
flag-cz	🇨🇿
flag-de	🇩🇪
flag-dg	🇩🇬
flag-dj	🇩🇯
flag-dk	🇩🇰
flag-dm	🇩🇲
flag-do	🇩🇴
flag-dz	🇩🇿
flag-ea	🇪🇦
flag-ec	🇪🇨
flag-ee	🇪🇪
flag-eg	🇪🇬
flag-eh	🇪🇭
flag-er	🇪🇷
flag-es	🇪🇸
flag-et	🇪🇹
flag-eu	🇪🇺
flag-fi	🇫🇮
flag-fj	🇫🇯
flag-fk	🇫🇰
flag-fm	🇫🇲
flag-fo	🇫🇴
flag-fr	🇫🇷
flag-ga	🇬🇦
flag-gb	🇬🇧
flag-gd	🇬🇩
flag-ge	🇬🇪
flag-gf	🇬🇫
flag-gg	🇬🇬
flag-gh	🇬🇭
flag-gi	🇬🇮
flag-gl	🇬🇱
flag-gm	🇬🇲
flag-gn	🇬🇳
flag-gp	🇬🇵
flag-gq	🇬🇶
flag-gr	🇬🇷
flag-gs	🇬🇸
flag-gt	🇬🇹
flag-gu	🇬🇺
flag-gw	🇬🇼
flag-gy	🇬🇾
flag-hk	🇭🇰
flag-hm	🇭🇲
flag-hn	🇭🇳
flag-hr	🇭🇷
flag-ht	🇭🇹
flag-hu	🇭🇺
flag-ic	🇮🇨
flag-id	🇮🇩
flag-ie	🇮🇪
flag-il	🇮🇱
flag-im	🇮🇲
flag-in	🇮🇳
flag-io	🇮🇴
flag-iq	🇮🇶
flag-ir	🇮🇷
flag-is	🇮🇸
flag-it	🇮🇹
flag-je	🇯🇪
flag-jm	🇯🇲
flag-jo	🇯🇴
flag-jp	🇯🇵
flag-ke	🇰🇪
flag-kg	🇰🇬
flag-kh	🇰🇭
flag-ki	🇰🇮
flag-km	🇰🇲
flag-kn	🇰🇳
flag-kp	🇰🇵
flag-kr	🇰🇷
flag-kw	🇰🇼
flag-ky	🇰🇾
flag-kz	🇰🇿
flag-la	🇱🇦
flag-lb	🇱🇧
flag-lc	🇱🇨
flag-li	🇱🇮
flag-lk	🇱🇰
flag-lr	🇱🇷
flag-ls	🇱🇸
flag-lt	🇱🇹
flag-lu	🇱🇺
flag-lv	🇱🇻
flag-ly	🇱🇾
flag-ma	🇲🇦
flag-mc	🇲🇨
flag-md	🇲🇩
flag-me	🇲🇪
flag-mf	🇲🇫
flag-mg	🇲🇬
flag-mh	🇲🇭
flag-mk	🇲🇰
flag-ml	🇲🇱
flag-mm	🇲🇲
flag-mn	🇲🇳
flag-mo	🇲🇴
flag-mp	🇲🇵
flag-mq	🇲🇶
flag-mr	🇲🇷
flag-ms	🇲🇸
flag-mt	🇲🇹
flag-mu	🇲🇺
flag-mv	🇲🇻
flag-mw	🇲🇼
flag-mx	🇲🇽
flag-my	🇲🇾
flag-mz	🇲🇿
flag-na	🇳🇦
flag-nc	🇳🇨
flag-ne	🇳🇪
flag-nf	🇳🇫
flag-ng	🇳🇬
flag-ni	🇳🇮
flag-nl	🇳🇱
flag-no	🇳🇴
flag-np	🇳🇵
flag-nr	🇳🇷
flag-nu	🇳🇺
flag-nz	🇳🇿
flag-om	🇴🇲
flag-pa	🇵🇦
flag-pe	🇵🇪
flag-pf	🇵🇫
flag-pg	🇵🇬
flag-ph	🇵🇭
flag-pk	🇵🇰
flag-pl	🇵🇱
flag-pm	🇵🇲
flag-pn	🇵🇳
flag-pr	🇵🇷
flag-ps	🇵🇸
flag-pt	🇵🇹
flag-pw	🇵🇼
flag-py	🇵🇾
flag-qa	🇶🇦
flag-re	🇷🇪
flag-ro	🇷🇴
flag-rs	🇷🇸
flag-ru	🇷🇺
flag-rw	🇷🇼
flag-sa	🇸🇦
flag-sb	🇸🇧
flag-sc	🇸🇨
flag-sd	🇸🇩
flag-se	🇸🇪
flag-sg	🇸🇬
flag-sh	🇸🇭
flag-si	🇸🇮
flag-sj	🇸🇯
flag-sk	🇸🇰
flag-sl	🇸🇱
flag-sm	🇸🇲
flag-sn	🇸🇳
flag-so	🇸🇴
flag-sr	🇸🇷
flag-ss	🇸🇸
flag-st	🇸🇹
flag-sv	🇸🇻
flag-sx	🇸🇽
flag-sy	🇸🇾
flag-sz	🇸🇿
flag-ta	🇹🇦
flag-tc	🇹🇨
flag-td	🇹🇩
flag-tf	🇹🇫
flag-tg	🇹🇬
flag-th	🇹🇭
flag-tj	🇹🇯
flag-tk	🇹🇰
flag-tl	🇹🇱
flag-tm	🇹🇲
flag-tn	🇹🇳
flag-to	🇹🇴
flag-tr	🇹🇷
flag-tt	🇹🇹
flag-tv	🇹🇻
flag-tw	🇹🇼
flag-tz	🇹🇿
flag-ua	🇺🇦
flag-ug	🇺🇬
flag-um	🇺🇲
flag-un	🇺🇳
flag-us	🇺🇸
flag-uy	🇺🇾
flag-uz	🇺🇿
flag-va	🇻🇦
flag-vc	🇻🇨
flag-ve	🇻🇪
flag-vg	🇻🇬
flag-vi	🇻🇮
flag-vn	🇻🇳
flag-vu	🇻🇺
flag-wf	🇼🇫
flag-ws	🇼🇸
flag-xk	🇽🇰
flag-ye	🇾🇪
flag-yt	🇾🇹
flag-za	🇿🇦
flag-zm	🇿🇲
flag-zw	🇿🇼
flags	🎏
flashlight	🔦
fleur de lis	⚜
floppy disk	💾
flower playing cards	🎴
flushed	😳
fog	🌫
foggy	🌁
football	🏈
footprints	👣
fork and knife	🍴
fountain	⛲
four	4⃣
four leaf clover	🍀
fox face	🦊
frame with picture	🖼
free	🆓
fried egg	🍳
fried shrimp	🍤
fries	🍟
frog	🐸
frowning	😦
fuelpump	⛽
full moon	🌕
full moon with face	🌝
funeral urn	⚱
game die	🎲
gear	⚙
gem	💎
gemini	♊
ghost	👻
gift	🎁
gift heart	💝
girl	👧
glass of milk	🥛
globe with meridians	🌐
goal net	🥅
goat	🐐
golf	⛳
golfer	🏌
gorilla	🦍
grapes	🍇
green apple	🍏
green book	📗
green heart	💚
green salad	🥗
grey exclamation	❕
grey question	❔
grimacing	😬
grin	😁
grinning	😀
guardsman	💂
guitar	🎸
gun	🔫
haircut	💇
hamburger	🍔
hammer	🔨
hammer and pick	⚒
hammer and wrench	🛠
hamster	🐹
hand	✋
hand with index and middle fingers crossed	🤞
handbag	👜
handball	🤾
handshake	🤝
hankey	💩
hash	#⃣
hatched chick	🐥
hatching chick	🐣
headphones	🎧
hear no evil	🙉
heart	❤
heart decoration	💟
heart eyes	😍
heart eyes cat	😻
heartbeat	💓
heartpulse	💗
hearts	♥
heavy check mark	✔
heavy division sign	➗
heavy dollar sign	💲
heavy heart exclamation mark ornament	❣
heavy minus sign	➖
heavy multiplication x	✖
heavy plus sign	➕
helicopter	🚁
helmet with white cross	⛑
herb	🌿
hibiscus	🌺
high brightness	🔆
high heel	👠
hocho	🔪
hole	🕳
honey pot	🍯
horse	🐴
horse racing	🏇
hospital	🏥
hot pepper	🌶
hotdog	🌭
hotel	🏨
hotsprings	♨
hourglass	⌛
hourglass flowing sand	⏳
house	🏠
house buildings	🏘
house with garden	🏡
hugging face	🤗
hushed	😯
ice cream	🍨
ice hockey stick and puck	🏒
ice skate	⛸
icecream	🍦
id	🆔
ideograph advantage	🉐
imp	👿
inbox tray	📥
incoming envelope	📨
information desk person	💁
information source	ℹ
innocent	😇
interrobang	⁉
iphone	📱
izakaya lantern	🏮
jack o lantern	🎃
japan	🗾
japanese castle	🏯
japanese goblin	👺
japanese ogre	👹
jeans	👖
joy	😂
joy cat	😹
joystick	🕹
juggling	🤹
kaaba	🕋
key	🔑
keyboard	⌨
keycap star	*⃣
keycap ten	🔟
kimono	👘
kiss	💋
kissing	😗
kissing cat	😽
kissing closed eyes	😚
kissing heart	😘
kissing smiling eyes	😙
kiwifruit	🥝
knife fork plate	🍽
koala	🐨
koko	🈁
label	🏷
large blue circle	🔵
large blue diamond	🔷
large orange diamond	🔶
last quarter moon	🌗
last quarter moon with face	🌜
latin cross	✝
laughing	😆
leaves	🍃
ledger	📒
left luggage	🛅
left right arrow	↔
left speech bubble	🗨
left-facing fist	🤛
leftwards arrow with hook	↩
lemon	🍋
leo	♌
leopard	🐆
level slider	🎚
libra	♎
light rail	🚈
lightning	🌩
link	🔗
linked paperclips	🖇
lion face	🦁
lips	👄
lipstick	💄
lizard	🦎
lock	🔒
lock with ink pen	🔏
lollipop	🍭
loop	➿
loud sound	🔊
loudspeaker	📢
love hotel	🏩
love letter	💌
low brightness	🔅
lower left ballpoint pen	🖊
lower left crayon	🖍
lower left fountain pen	🖋
lower left paintbrush	🖌
lying face	🤥
m	Ⓜ
mag	🔍
mag right	🔎
mahjong	🀄
mailbox	📫
mailbox closed	📪
mailbox with mail	📬
mailbox with no mail	📭
male sign	♂
male-artist	👨‍🎨
male-astronaut	👨‍🚀
male-construction-worker	👷‍♂️
male-cook	👨‍🍳
male-detective	🕵️‍♂️
male-doctor	👨‍⚕️
male-factory-worker	👨‍🏭
male-farmer	👨‍🌾
male-firefighter	👨‍🚒
male-guard	💂‍♂️
male-judge	👨‍⚖️
male-mechanic	👨‍🔧
male-office-worker	👨‍💼
male-pilot	👨‍✈️
male-police-officer	👮‍♂️
male-scientist	👨‍🔬
male-singer	👨‍🎤
male-student	👨‍🎓
male-teacher	👨‍🏫
male-technologist	👨‍💻
man	👨
man dancing	🕺
man in business suit levitating	🕴
man in tuxedo	🤵
man with gua pi mao	👲
man with turban	👳
man-biking	🚴‍♂️
man-bouncing-ball	⛹️‍♂️
man-bowing	🙇‍♂️
man-boy	👨‍👦
man-boy-boy	👨‍👦‍👦
man-cartwheeling	🤸‍♂️
man-facepalming	🤦‍♂️
man-frowning	🙍‍♂️
man-gesturing-no	🙅‍♂️
man-gesturing-ok	🙆‍♂️
man-getting-haircut	💇‍♂️
man-getting-massage	💆‍♂️
man-girl	👨‍👧
man-girl-boy	👨‍👧‍👦
man-girl-girl	👨‍👧‍👧
man-golfing	🏌️‍♂️
man-heart-man	👨‍❤️‍👨
man-juggling	🤹‍♂️
man-kiss-man	👨‍❤️‍💋‍👨
man-lifting-weights	🏋️‍♂️
man-man-boy	👨‍👨‍👦
man-man-boy-boy	👨‍👨‍👦‍👦
man-man-girl	👨‍👨‍👧
man-man-girl-boy	👨‍👨‍👧‍👦
man-man-girl-girl	👨‍👨‍👧‍👧
man-mountain-biking	🚵‍♂️
man-playing-handball	🤾‍♂️
man-playing-water-polo	🤽‍♂️
man-pouting	🙎‍♂️
man-raising-hand	🙋‍♂️
man-rowing-boat	🚣‍♂️
man-running	🏃‍♂️
man-shrugging	🤷‍♂️
man-surfing	🏄‍♂️
man-swimming	🏊‍♂️
man-tipping-hand	💁‍♂️
man-walking	🚶‍♂️
man-wearing-turban	👳‍♂️
man-with-bunny-ears-partying	👯‍♂️
man-woman-boy	👨‍👩‍👦
man-woman-boy-boy	👨‍👩‍👦‍👦
man-woman-girl	👨‍👩‍👧
man-woman-girl-boy	👨‍👩‍👧‍👦
man-woman-girl-girl	👨‍👩‍👧‍👧
man-wrestling	🤼‍♂️
mans shoe	👞
mantelpiece clock	🕰
maple leaf	🍁
martial arts uniform	🥋
mask	😷
massage	💆
meat on bone	🍖
medal	🎖
mega	📣
melon	🍈
memo	📝
menorah with nine branches	🕎
mens	🚹
metro	🚇
microphone	🎤
microscope	🔬
middle finger	🖕
milky way	🌌
minibus	🚐
minidisc	💽
mobile phone off	📴
money mouth face	🤑
money with wings	💸
moneybag	💰
monkey	🐒
monkey face	🐵
monorail	🚝
moon	🌔
mortar board	🎓
mosque	🕌
mostly sunny	🌤
mother christmas	🤶
motor boat	🛥
motor scooter	🛵
motorway	🛣
mount fuji	🗻
mountain	⛰
mountain bicyclist	🚵
mountain cableway	🚠
mountain railway	🚞
mouse	🐭
mouse2	🐁
movie camera	🎥
moyai	🗿
muscle	💪
mushroom	🍄
musical keyboard	🎹
musical note	🎵
musical score	🎼
mute	🔇
nail care	💅
name badge	📛
national park	🏞
nauseated face	🤢
necktie	👔
negative squared cross mark	❎
nerd face	🤓
neutral face	😐
new	🆕
new moon	🌑
new moon with face	🌚
newspaper	📰
ng	🆖
night with stars	🌃
nine	9⃣
no bell	🔕
no bicycles	🚳
no entry	⛔
no entry sign	🚫
no good	🙅
no mobile phones	📵
no mouth	😶
no pedestrians	🚷
no smoking	🚭
non-potable water	🚱
nose	👃
notebook	📓
notebook with decorative cover	📔
notes	🎶
nut and bolt	🔩
o	⭕
o2	🅾
ocean	🌊
octagonal sign	🛑
octopus	🐙
oden	🍢
office	🏢
oil drum	🛢
ok	🆗
ok hand	👌
ok woman	🙆
old key	🗝
older man	👴
older woman	👵
om symbol	🕉
on	🔛
oncoming automobile	🚘
oncoming bus	🚍
oncoming police car	🚔
oncoming taxi	🚖
one	1⃣
open file folder	📂
open hands	👐
open mouth	😮
ophiuchus	⛎
orange book	📙
orthodox cross	☦
outbox tray	📤
owl	🦉
ox	🐂
package	📦
page facing up	📄
page with curl	📃
pager	📟
palm tree	🌴
pancakes	🥞
panda face	🐼
paperclip	📎
parking	🅿
part alternation mark	〽
partly sunny	⛅
partly sunny rain	🌦
passenger ship	🛳
passport control	🛂
peace symbol	☮
peach	🍑
peanuts	🥜
pear	🍐
pencil2	✏
penguin	🐧
pensive	😔
performing arts	🎭
persevere	😣
person doing cartwheel	🤸
person frowning	🙍
person with ball	⛹
person with blond hair	👱
person with pouting face	🙎
phone	☎
pick	⛏
pig	🐷
pig nose	🐽
pig2	🐖
pill	💊
pineapple	🍍
pisces	♓
pizza	🍕
place of worship	🛐
point down	👇
point left	👈
point right	👉
point up	☝
point up 2	👆
police car	🚓
poodle	🐩
popcorn	🍿
post office	🏣
postal horn	📯
postbox	📮
potable water	🚰
potato	🥔
pouch	👝
poultry leg	🍗
pound	💷
pouting cat	😾
pray	🙏
prayer beads	📿
pregnant woman	🤰
prince	🤴
princess	👸
printer	🖨
purple heart	💜
purse	👛
pushpin	📌
put litter in its place	🚮
question	❓
rabbit	🐰
rabbit2	🐇
racehorse	🐎
racing car	🏎
racing motorcycle	🏍
radio	📻
radio button	🔘
radioactive sign	☢
rage	😡
railway car	🚃
railway track	🛤
rain cloud	🌧
rainbow	🌈
rainbow-flag	🏳️‍🌈
raised back of hand	🤚
raised hand with fingers splayed	🖐
raised hands	🙌
raising hand	🙋
ram	🐏
ramen	🍜
rat	🐀
recycle	♻
red circle	🔴
registered	®
relaxed	☺
relieved	😌
reminder ribbon	🎗
repeat	🔁
repeat one	🔂
restroom	🚻
revolving hearts	💞
rewind	⏪
rhinoceros	🦏
ribbon	🎀
rice	🍚
rice ball	🍙
rice cracker	🍘
rice scene	🎑
right anger bubble	🗯
right-facing fist	🤜
ring	💍
robot face	🤖
rocket	🚀
rolled up newspaper	🗞
roller coaster	🎢
rolling on the floor laughing	🤣
rooster	🐓
rose	🌹
rosette	🏵
rotating light	🚨
round pushpin	📍
rowboat	🚣
rugby football	🏉
runner	🏃
running shirt with sash	🎽
sa	🈂
sagittarius	♐
sake	🍶
sandal	👡
santa	🎅
satellite	🛰
satellite antenna	📡
saxophone	🎷
scales	⚖
school	🏫
school satchel	🎒
scissors	✂
scooter	🛴
scorpion	🦂
scorpius	♏
scream	😱
scream cat	🙀
scroll	📜
seat	💺
second place medal	🥈
secret	㊙
see no evil	🙈
seedling	🌱
selfie	🤳
seven	7⃣
shallow pan of food	🥘
shamrock	☘
shark	🦈
shaved ice	🍧
sheep	🐑
shell	🐚
shield	🛡
shinto shrine	⛩
ship	🚢
shirt	👕
shopping bags	🛍
shopping trolley	🛒
shower	🚿
shrimp	🦐
shrug	🤷
signal strength	📶
six	6⃣
six pointed star	🔯
ski	🎿
skier	⛷
skin-tone-2	🏻
skin-tone-3	🏼
skin-tone-4	🏽
skin-tone-5	🏾
skin-tone-6	🏿
skull	💀
skull and crossbones	☠
sleeping	😴
sleeping accommodation	🛌
sleepy	😪
sleuth or spy	🕵
slightly frowning face	🙁
slightly smiling face	🙂
slot machine	🎰
small airplane	🛩
small blue diamond	🔹
small orange diamond	🔸
small red triangle	🔺
small red triangle down	🔻
smile	😄
smile cat	😸
smiley	😃
smiley cat	😺
smiling imp	😈
smirk	😏
smirk cat	😼
smoking	🚬
snail	🐌
snake	🐍
sneezing face	🤧
snow capped mountain	🏔
snow cloud	🌨
snowboarder	🏂
snowflake	❄
snowman	☃
snowman without snow	⛄
sob	😭
soccer	⚽
soon	🔜
sos	🆘
sound	🔉
space invader	👾
spades	♠
spaghetti	🍝
sparkle	❇
sparkler	🎇
sparkles	✨
sparkling heart	💖
speak no evil	🙊
speaker	🔈
speaking head in silhouette	🗣
speech balloon	💬
speedboat	🚤
spider	🕷
spider web	🕸
spiral calendar pad	🗓
spiral note pad	🗒
spock-hand	🖖
spoon	🥄
sports medal	🏅
squid	🦑
stadium	🏟
staff of aesculapius	⚕
star	⭐
star and crescent	☪
star of david	✡
star2	🌟
stars	🌠
station	🚉
statue of liberty	🗽
steam locomotive	🚂
stew	🍲
stopwatch	⏱
straight ruler	📏
strawberry	🍓
stuck out tongue	😛
stuck out tongue closed eyes	😝
stuck out tongue winking eye	😜
studio microphone	🎙
stuffed flatbread	🥙
sun with face	🌞
sunflower	🌻
sunglasses	😎
sunny	☀
sunrise	🌅
sunrise over mountains	🌄
surfer	🏄
sushi	🍣
suspension railway	🚟
sweat	😓
sweat drops	💦
sweat smile	😅
sweet potato	🍠
swimmer	🏊
symbols	🔣
synagogue	🕍
syringe	💉
table tennis paddle and ball	🏓
taco	🌮
tada	🎉
tanabata tree	🎋
tangerine	🍊
taurus	♉
taxi	🚕
tea	🍵
telephone receiver	📞
telescope	🔭
tennis	🎾
tent	⛺
the horns	🤘
thermometer	🌡
thinking face	🤔
third place medal	🥉
thought balloon	💭
three	3⃣
three button mouse	🖱
thunder cloud and rain	⛈
ticket	🎫
tiger	🐯
tiger2	🐅
timer clock	⏲
tired face	😫
tm	™
toilet	🚽
tokyo tower	🗼
tomato	🍅
tongue	👅
top	🔝
tophat	🎩
tornado	🌪
trackball	🖲
tractor	🚜
traffic light	🚥
train	🚋
train2	🚆
tram	🚊
triangular flag on post	🚩
triangular ruler	📐
trident	🔱
triumph	😤
trolleybus	🚎
trophy	🏆
tropical drink	🍹
tropical fish	🐠
truck	🚚
trumpet	🎺
tulip	🌷
tumbler glass	🥃
turkey	🦃
turtle	🐢
tv	📺
twisted rightwards arrows	🔀
two	2⃣
two hearts	💕
two men holding hands	👬
two women holding hands	👭
u5272	🈹
u5408	🈴
u55b6	🈺
u6307	🈯
u6708	🈷
u6709	🈶
u6e80	🈵
u7121	🈚
u7533	🈸
u7981	🈲
u7a7a	🈳
umbrella	☂
umbrella on ground	⛱
umbrella with rain drops	☔
unamused	😒
underage	🔞
unicorn face	🦄
unlock	🔓
up	🆙
upside down face	🙃
v	✌
vertical traffic light	🚦
vhs	📼
vibration mode	📳
video camera	📹
video game	🎮
violin	🎻
virgo	♍
volcano	🌋
volleyball	🏐
vs	🆚
walking	🚶
waning crescent moon	🌘
waning gibbous moon	🌖
warning	⚠
wastebasket	🗑
watch	⌚
water buffalo	🐃
water polo	🤽
watermelon	🍉
wave	👋
waving black flag	🏴
waving white flag	🏳
wavy dash	〰
waxing crescent moon	🌒
wc	🚾
weary	😩
wedding	💒
weight lifter	🏋
whale	🐳
whale2	🐋
wheel of dharma	☸
wheelchair	♿
white check mark	✅
white circle	⚪
white flower	💮
white frowning face	☹
white large square	⬜
white medium small square	◽
white medium square	◻
white small square	▫
white square button	🔳
wilted flower	🥀
wind blowing face	🌬
wind chime	🎐
wine glass	🍷
wink	😉
wolf	🐺
woman	👩
woman-biking	🚴‍♀️
woman-bouncing-ball	⛹️‍♀️
woman-bowing	🙇‍♀️
woman-boy	👩‍👦
woman-boy-boy	👩‍👦‍👦
woman-cartwheeling	🤸‍♀️
woman-facepalming	🤦‍♀️
woman-frowning	🙍‍♀️
woman-gesturing-no	🙅‍♀️
woman-gesturing-ok	🙆‍♀️
woman-getting-haircut	💇‍♀️
woman-getting-massage	💆‍♀️
woman-girl	👩‍👧
woman-girl-boy	👩‍👧‍👦
woman-girl-girl	👩‍👧‍👧
woman-golfing	🏌️‍♀️
woman-heart-man	👩‍❤️‍👨
woman-heart-woman	👩‍❤️‍👩
woman-juggling	🤹‍♀️
woman-kiss-man	👩‍❤️‍💋‍👨
woman-kiss-woman	👩‍❤️‍💋‍👩
woman-lifting-weights	🏋️‍♀️
woman-mountain-biking	🚵‍♀️
woman-playing-handball	🤾‍♀️
woman-playing-water-polo	🤽‍♀️
woman-pouting	🙎‍♀️
woman-raising-hand	🙋‍♀️
woman-rowing-boat	🚣‍♀️
woman-running	🏃‍♀️
woman-shrugging	🤷‍♀️
woman-surfing	🏄‍♀️
woman-swimming	🏊‍♀️
woman-tipping-hand	💁‍♀️
woman-walking	🚶‍♀️
woman-wearing-turban	👳‍♀️
woman-with-bunny-ears-partying	👯‍♀️
woman-woman-boy	👩‍👩‍👦
woman-woman-boy-boy	👩‍👩‍👦‍👦
woman-woman-girl	👩‍👩‍👧
woman-woman-girl-boy	👩‍👩‍👧‍👦
woman-woman-girl-girl	👩‍👩‍👧‍👧
woman-wrestling	🤼‍♀️
womans clothes	👚
womans hat	👒
womens	🚺
world map	🗺
worried	😟
wrench	🔧
wrestlers	🤼
writing hand	✍
x	❌
yellow heart	💛
yen	💴
yin yang	☯
yum	😋
zap	⚡
zero	0⃣
zipper mouth face	🤐
zzz	💤
"

line=$(echo -n "$emoji_list" "$textface_list" | rofi -mesg "Choose an emoji!" -dmenu -i);
emoji=$(echo -n "$line" | cut -f2 -);
echo -n "$emoji" | xdotool type --file -
